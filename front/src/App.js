import './App.css';
import { useState, useEffect } from 'react';
import RadialTree from './RadialTree';
import { TextInput, Button, Loader, Slider, Text } from '@mantine/core';
import { Webhook } from 'tabler-icons-react';

function App() {
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(false);

  const [value, setValue] = useState('');

  const [DF, setDF] = useState(0.85);

  const [PRI, setPRI] = useState(20);

  const makeAPICall = async (value) => {
    try {
      console.log(value);
      if (value.length < 1) {
        return;
      }
      setLoading(true);

      // fetch the api with body
      // allow cors for this call
      const response = await fetch('http://localhost:1515/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*',
        },
        body: JSON.stringify({
          url: value,
          df: DF,
          pri: PRI,
        })
      });
      const response_json = await response.json();
      setData(response_json);
      setLoading(false);
    }
    catch (e) {
      console.log("ALED", e)
    }
  }




  const displayGraph = (graph) => {


    return (
      <>
        Click the root node to expand the tree!

        {<RadialTree data={graph} width={1800} height={1800} />}
      </>

    )
  }




  useEffect(() => {
  }, [])

  return (
    <div className="App">
      <header className="App-header">
        <div className='container'>

          <TextInput icon={<Webhook />}
            placeholder="The website to crawl"
            size="lg"
            value={value}
            onChange={(event) => setValue(event.currentTarget.value)}
          />

          <div>
            <div className="slider-container">
              <Text size="xl"> Damping factor </Text>
              <Slider
                size="xl"
                radius="xl"
                min={0}
                max={1}
                step={0.01}
                label={(value) => value.toFixed(2)}
                value={DF}
                onChange={setDF}
                marks={[
                  { value: 0, label: '0' },
                  { value: 1, label: '1' },
                  { value: 0.85, label: '0.85' },
                ]}
              />
              <Text size="xl"> pagerank iterations </Text>
              <Slider
                size="xl"
                radius="xl"
                value={PRI}
                onChange={setPRI}
                min={1}
                max={100}
                marks={[
                  { value: 1, label: '1' },
                  { value: 100, label: '100' },
                  { value: 20, label: '20' },
                ]}
              />

            </div>
            {!loading && <Button color="indigo" radius="xl" size="lg" onClick={() => { makeAPICall(value) }}>
              Crawl it!
            </Button>
            }
            {
              loading && <Loader size="xl" variant="bars" />
            }

          </div>

        </div>
        {!loading && data && displayGraph(data)}
      </header>
    </div >
  );
}

export default App;
