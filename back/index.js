import express from 'express'
const app = express()
const port = 1515
import scrapeIt from "scrape-it"
import normalizeUrl from 'normalize-url'
import cors from 'cors';

import bodyParser from 'body-parser';
app.use(bodyParser.json());


app.use(cors());

const MAX_PROMISES = 10;

const get_outgoing_links = async (current_url, base_url, next_tree_parent) => {
    let urls_found_in_page = [];

    let scraped = await scrapeIt(current_url, {
        pages: {
            listItem: "a"
            , data: {
                links: {
                    attr: "href"
                }
            }
        }
    });

    scraped.data.pages.forEach(element => {

        let link = element.links

        if (link.startsWith("#"))
            return
        if (!link.startsWith(base_url)) {
            if (link.startsWith("http"))
                return
            link = base_url + link
        }

        link = normalizeUrl(link)
        urls_found_in_page.push(link)

    });
    return { urls_found_in_page, current_url, next_tree_parent };
}


const pagerank = (map, tree, PRI, DF) => {
    // calculer le page rank de chaque element de la map
    let total_pages = Object.keys(map).length;
    // appliquer ce pagerank à chaque node de l'arbre
    let stack = []

    stack.push(tree);

    let base_pr = 1 / total_pages;

    while (stack.length > 0) {
        let node = stack.pop();
        map[node.name].page_rank = base_pr;
        node.page_rank = base_pr;
        node.children.forEach(child => {
            stack.push(child);
        })
    }

    console.time("algo")

    for (let i = 0; i < PRI; i++) {
        stack.push(tree);

        while (stack.length > 0) {
            let node = stack.pop();


            let pagerank = (1 - DF) / total_pages + DF * map[node.name].parents.reduce((acc, parent) => {
                return acc + map[parent].page_rank / map[parent].children.length;
            }, 0);
            node.page_rank = pagerank
            map[node.name].page_rank = base_pr;
            node.children.forEach(child => {
                stack.push(child);
            })
        }
    }

    console.timeEnd("algo")

}


const just_scrape_it = async (start_url, base_url, map, tree) => {
    let stack = []

    stack.push([start_url, tree]);

    let current_url;
    let tree_parent;
    while (stack.length > 0) {

        let node_children = []

        for (let i = 0; i < MAX_PROMISES && stack.length > 0; i++) {
            let poped = stack.pop();
            current_url = poped[0];
            tree_parent = poped[1];


            if (!map[current_url]) {
                map[current_url] = { children: [], parents: [], visited: true };
            }
            else {
                map[current_url].visited = true;
            }

            let next_tree_parent = { name: current_url, children: [] };
            tree_parent.children.push(next_tree_parent);

            node_children.push(get_outgoing_links(current_url, base_url, next_tree_parent));
        }

        await Promise.all(node_children).then(meta_list => {
            meta_list.forEach(meta => {
                meta.urls_found_in_page.forEach(child => {
                    map[meta.current_url].children.push(child);
                    if (!map[child]) {
                        map[child] = { children: [], parents: [], visited: false };
                    }
                    if (!map[child].visited) {
                        stack.push([child, meta.next_tree_parent]);
                    }
                    map[child].parents.push(meta.current_url)
                })
            })
        })


    }
}


app.post('/', async (req, res) => {
    res.header("Access-Control-Allow-Origin", "*");
    // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    // get the url from the body
    let url = req.body.url;

    let PRI = req.body.PRI;
    let DF = req.body.DF;

    let start_url = url

    let base_url = url

    if (!base_url.endsWith("/")) {
        base_url += "/"
    }


    let map = new Map();
    let tree = { name: "root", children: [] };

    await just_scrape_it(start_url, base_url, map, tree);
    console.log("finished scraping")
    tree = tree.children[0]
    pagerank(map, tree, PRI, DF);

    res.send(tree);


})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})
