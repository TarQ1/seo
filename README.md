# SEO: Pagerank

## Dokerised run

To start the website and the backend please type `docker-compose up` and visit `localhost:3002`

## Direct Run

If you don't have `docker-compose`, please go to `./back` and type `npm i` followed by `npm run start`

Likewise, go to `./front` and type `npm i` followed by `npm run start`

Next visit `localhost:3000`
